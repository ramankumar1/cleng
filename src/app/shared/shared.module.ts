import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { HeaderComponent } from './layout/header/header.component';
import { RouterModule } from '@angular/router';
import { CompareDirective } from './directives/compare.directive';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { SearchFilterPipe } from './pipes/searchFilter.pipe';
import { DatePipe } from '@angular/common';
import { EmbedVideo } from 'ngx-embed-video';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from "@angular/material/table";
import { ErrorComponent } from './error/error.component';
import { UnauthorisedComponent } from './unauthorised/unauthorised.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgApexchartsModule } from "ng-apexcharts";
import { SafeUrlPipe } from './pipes/safe.pipe';
import { NgxSpinnerModule } from 'ngx-spinner';
import { QRCodeModule } from 'angularx-qrcode';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [SidebarComponent, HeaderComponent, CompareDirective, SearchFilterPipe,
    ErrorComponent, UnauthorisedComponent, SafeUrlPipe
    //  ProgressBarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    CarouselModule,
    EmbedVideo.forRoot(),
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    NgApexchartsModule,
    ModalModule.forRoot(),
    NgxSpinnerModule,
    QRCodeModule,
    NgxQRCodeModule,

  ],
  exports: [
    FormsModule,
    SidebarComponent,
    RouterModule,
    HeaderComponent,
    CompareDirective,
    CarouselModule,
    SearchFilterPipe,
    DatePipe,
    EmbedVideo,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    ModalModule,
    NgApexchartsModule,
    SafeUrlPipe,
    NgxSpinnerModule,
    QRCodeModule,
    NgxQRCodeModule,
    QRCodeModule,
    NgxQRCodeModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedModule { }
