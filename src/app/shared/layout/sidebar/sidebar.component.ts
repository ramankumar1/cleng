import { Component, OnInit, Output, Input, ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { NavigationExtras, Router } from '@angular/router';
import * as moment from 'moment';
import { QrScannerComponent } from 'src/app/modules/auth/qr-scanner/qr-scanner.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  hide: boolean = true;
  @Input() showsidebar: any;
  @Input() crossIcon: any;
  @Input() barIcon: any;
  @Input() overlay: any;

  @Output() dataSend = new EventEmitter();
  emitStoreValues: any;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  userName: any;
  accountType: any;

  ngOnInit(): void {

    const userAccount = this.localStorageService.getUserCredentials();
    this.userName = userAccount.userName;
    this.accountType = userAccount.accountType.accountTypeName;
  }

  reportWizard() {
    this.router.navigateByUrl("/admin/report-wizard");
    window.scrollTo(0, 0);
  }

  prescriptiveMaintenance() {
    window.scrollTo(0, 0);
  }

  logout() {
    this.localStorageService.logout();
    this.router.navigateByUrl("/login");
  }

  hideClass() {
    this.showsidebar = false,
    this.crossIcon = false,
    this.barIcon = true,
    this.overlay = false

    this.dataSend.emit(this.emitStoreValues)
  }

  twelveMonthOverViewReport(printCSV = false) {
    const { companyId, engineerName, reportDate, companyReportDetailId, collectedBy } = this.localStorageService.getstoreTwelveMonthData();
    const companyDetail = this.localStorageService.getCompanyId();
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "companyId": companyDetail.companyId,
        "reportDate": reportDate,
        "engineerName": engineerName,
        "companyReportDetailId": companyReportDetailId,
        "collectedBy": collectedBy,
        "companyName": companyDetail.companyName,
        "print": !printCSV,
        "backUrl": 'dashboard',
        "overAllReport": true,
        "csv": printCSV
      }
    };
    this.router.navigate(["/admin/full-cm-report-detail"], navigationExtras);
  }

  qrScanCode() {
    // const dialogRef = this.dialog.open(QrScannerComponent, {
    //   width: '650px',
    //   disableClose: true
    // });
   }

}
