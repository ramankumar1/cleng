import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { NavigationEnd, Router } from '@angular/router';
import { GlobalCodeService } from 'src/app/core/services/global-code.service';
import { MatDialog } from '@angular/material/dialog';
import { QrScannerComponent } from 'src/app/modules/auth/qr-scanner/qr-scanner.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  showSidebar: boolean = false;
  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private globalService: GlobalCodeService,
    public dialog: MatDialog
  ) { }
  userName: any;

  overlay: boolean = false;
  crossIcon: boolean = false;
  barIcon: boolean = true;

  ngOnInit(): void {
    const user = this.localStorageService.getUserCredentials();
    this.userName = user.userName;
    // this.showOverlay();

  }

  searchFilter(event) {
    this.globalService.searchFilter.next(event.target.value);
  }

  logout() {
    this.localStorageService.logout();
    this.router.navigateByUrl("/login");
  }

  get isMobile() {
    return document.body.clientWidth < 991;
  }

  toggle() {
    if (this.showSidebar == false) {
      this.showSidebar = true;
      this.crossIcon = true;
      this.barIcon = false;
      this.overlay = true;
    } else {
      this.showSidebar = false;
      this.crossIcon = false;
      this.barIcon = true;
      this.overlay = false;
    }
  }

  showOverlay() {

  }

  dataSend(e: any) {
    this.showSidebar = false;
    this.crossIcon = false;
    this.barIcon = true;
    this.overlay = false;
  }

  qrScanCode() {
    // const dialogRef = this.dialog.open(QrScannerComponent, {
    //   width: '650px',
    //   disableClose: true
    // });
  }


  get hasPrescriptive() {
    return this.router.url == '/admin/prescriptive-maintenance';
  }


  // @HostListener('mouseenter', ['$event'])
  // onMouseEnter(event: any) {
  //   if (this.overlay) {
  //     this.showSidebar = false;
  //     this.crossIcon = false;
  //     this.barIcon = true;
  //     this.overlay = false;

  //   } else {
  //     this.crossIcon = false;
  //     this.showSidebar = true;
  //     this.crossIcon = true;
  //     this.barIcon = false;
  //     this.overlay = true;
  //   }
  // }

}
