import { NG_VALIDATORS, Validators, AbstractControl } from '@angular/forms';
import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[appCompareDirectives]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: CompareDirective,
    multi: true
  }] 
})
export class CompareDirective implements Validators {
  @Input() appCompareDirectives: string;
  validate(control: AbstractControl): { [key: string]: any } | null {
    const controlToCompare = control.parent.get(this.appCompareDirectives);
    if (controlToCompare && controlToCompare.value !== control.value) {
        return { 'notEqual': true };
    }

    return null;
}

}
