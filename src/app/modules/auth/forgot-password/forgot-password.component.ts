import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { AuthModel } from 'src/app/core/models/auth.model';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  pageLocation: any;
  path: any;
  pathExtension: any;
  forgotPassword: AuthModel = {
    email: null
  }
  constructor(
    private router: Router,
    private toastr: ToastrService,
    private authService: AuthService,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit(): void {
    this.path = "/reset-password";
    
    this.pageLocation = window.location.origin + this.path;
  }

  submit(form: NgForm){
    console.log(this.forgotPassword.email);
    if(form.valid){
      let query = {
        email: this.forgotPassword.email,
        url: this.pageLocation,
      }
      this.authService.sendForgotPasswordEmail(query).subscribe(res => {
        this.localStorageService.storeResetPasswordToken(res.userResponse.token);
        this.localStorageService.storeUserCredentials(res.email);
     
      this.toastr.success('', res.message, {
        timeOut: 2000
      });
      
      }, error => {
        
      })
    }
  }


}
