import { Component, OnInit } from '@angular/core';
import { AuthModel } from 'src/app/core/models/auth.model';
import { NgForm, NgModel } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  returnUrl : string;
  token : any;
  loginData: AuthModel = {
    email: null,
    password: null
  }

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private authService: AuthService,
    private localStorageService: LocalStorageService,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.returnUrl =
      this._activatedRoute.snapshot.queryParams["returnUrl"] || "/admin/dashboard";
    this.token = this.localStorageService.getAuthorizationToken();
    if(this.token !== null || this.token !== "" || this.token !== "undefine"){
      this.router.navigateByUrl(this.returnUrl);
    }
  }

  submit(form: NgForm){
    if(form.valid){
      this.authService.login(this.loginData).subscribe(res =>{
        this.localStorageService.storeAuthToken(res.userResponse.token);
        this.localStorageService.storeUserCredentials(res.userResponse);
        
        this.router.navigateByUrl(this.returnUrl);
        
        this.toastr.success('successfully', 'User LogIn', {
          timeOut: 2000
        });
      
      }, error => {
        this.toastr.error('', 'Username or password is incorrect', {
          timeOut: 2000
        });
      })
     
    }
    
  }

}
