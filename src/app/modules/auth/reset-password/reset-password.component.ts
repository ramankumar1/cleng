import { Component, OnInit } from '@angular/core';
import { AuthModel } from 'src/app/core/models/auth.model';
import { NgForm, NgModel } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  email: string;
  token: string;
  responseData: any;

  forgotPassword: AuthModel = {
    newPassword: null,
    confirmPassword: null
  }
  constructor(
    private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private localStorageService: LocalStorageService
    ) { }

  ngOnInit(): void {
    this.email = this.route.snapshot.queryParams.email
    this.token = this.route.snapshot.queryParams.token;
    let query = {
      email: this.email,
      token: this.token
    }
    this.authService.resetPasswordToken(query).subscribe(res => {
      this.responseData = res;
    }, error => {
      if (error = "Bad Request") {
        this.router.navigateByUrl("/error");
      }
    })
  }


  submit(form: NgForm){
    
    if(form.valid){
      let query = {
        email: this.route.snapshot.queryParams.email,
        newPassword: this.forgotPassword.newPassword,
        userName: ""
      }
      
      this.authService.changePassword(query).subscribe(res => {
        this.responseData = res;
        setTimeout(() =>{
          this.toastr.success('Successful', 'Password Reset', {
            timeOut: 2000
          });
          this.router.navigateByUrl("/login");
        }, 500)
        
      }, error => {
        if (error = "Bad Request") {
          this.router.navigateByUrl("/error");
        }
      })
      
    }
  }


}
