import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LayoutComponent } from './components/layout/layout.component';
import { SharedModule } from '../../shared/shared.module';
import { UserListComponent } from './components/user-list/user-list.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { CompanyComponent } from './components/company/company.component';
import { AddCompanyComponent } from './components/add-company/add-company.component';
import { ReportWizardComponent } from './components/report-wizard/report-wizard.component';
import { ShortCmReportComponent } from './components/short-cm-report/short-cm-report.component';
import { FullCmReportComponent } from './components/full-cm-report/full-cm-report.component';
import { ReportDetailsFormComponent } from './components/report-wizard/report-details-form/report-details-form.component';
import { ReportIssueFormComponent } from './components/report-wizard/report-issue-form/report-issue-form.component';
import { PrescriptiveMaintenanceComponent } from './components/prescriptive-maintenance/prescriptive-maintenance.component';
import { FullCmReportListComponent } from './components/full-cm-report-list/full-cm-report-list.component';
import { MainCmReportModalComponent } from './components/main-cm-report-modal/main-cm-report-modal.component';
import { MessagesComponent } from './components/messages/messages.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { FullCmReportDetailComponent } from './components/full-cm-report-detail/full-cm-report-detail.component';
import { BrgIdListComponent } from './components/brgId-list/brgId-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultipleShortCmReportComponent } from './components/short-cm-report/multiple-short-cm-report/multiple-short-cm-report.component';
import { ShortCmReportFormComponent } from './components/short-cm-report/short-cm-report-form/short-cm-report-form.component';


@NgModule({
  declarations: [
    DashboardComponent, 
    LayoutComponent, 
    UserListComponent ,
    AddUserComponent, 
    CompanyComponent, 
    AddCompanyComponent, 
    ReportWizardComponent, 
    ShortCmReportComponent, 
    FullCmReportComponent, 
    ReportDetailsFormComponent, 
    ReportIssueFormComponent, 
    PrescriptiveMaintenanceComponent,

    FullCmReportListComponent,
    MainCmReportModalComponent,
    MessagesComponent,
    ProgressBarComponent,
    FullCmReportDetailComponent,
    BrgIdListComponent,
    MultipleShortCmReportComponent,
    ShortCmReportFormComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class AdminModule { }
