import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment'
import { BaseUrl } from 'src/app/config/url-config';
import * as _ from 'lodash';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent implements OnInit {
  @Input('messageData') messageData: number;
  api = BaseUrl.apiUrl;
  msgInputTxt : '';
  msgList = [];
  messagesList: any[] = [];
  userId: number;
  notFound: boolean = true;
  constructor(
    private toastr: ToastrService,
    private messageServices: MessageService,
    private localStorageService: LocalStorageService,
    private SpinnerService: NgxSpinnerService,
    private route: ActivatedRoute,
  ) {  }
  
  companyDetailId: number = 0;
  companyId: number;


  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  imageType: string;
  assetId: string;
  companyNo: string;
  formData = new FormData();

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.companyDetailId = params.companyDetailId;
      this.companyId = params.companyId;
      this.assetId = params.assetId;
      this.companyNo =  params.companyNo
    })
    const userId = this.localStorageService.getUserCredentials();
    this.userId = userId.userId;
    this.getMessage();
  }

  getMessage(){
    const companyDetailId = Number(this.companyDetailId ? this.companyDetailId : 0);
    this.messageServices.getMessages({companyDetailId: companyDetailId, messageId:0, page:0, limit:0, orderBy:"CreatedOn", orderByDescending:true, allRecords: true}).subscribe(res => {
      this.messagesList = res.messageResponseList;
      if(this.messagesList && this.messagesList.length > 0){
        this.notFound = false;
      }else{
        this.notFound = true;
      }
    })
  }

  params = {
    messageFromId: 0,
    messageTitle: "string",
    messageDescription: "string",
    messageDate: "string",
    companyId: 0,
    companyDetailId: 0,
    messageId: 0,
    images: "string",
    imagesType: "string",
    actionBy: "string"
  }



  editMsgData : any;
  editMessage(object){
    
    const tempDate = moment().format();
    this.isImageSaved = true;
    this.msgInputTxt = object.messageDescription;
    this.cardImageBase64 = this.api + object.imagesResponse.imageUrl;
    this.params.messageFromId = this.userId;
    this.params.companyId = Number(this.companyId ? this.companyId : 0);
    this.params.companyDetailId = Number(this.companyDetailId ? this.companyDetailId : 0);
    this.params.messageId = object.messageId;
    this.params.messageDate = tempDate ;
    this.editMsgData = this.params;
    this.imageType = String(object.imagesResponse.imageType)
    this.imageUrl = object.imagesResponse.imageUrl
  }
  addMessage(){
    this.SpinnerService.show();

    const tempDate = moment().format();
    this.formData.append('messageDescription', this.msgInputTxt);
    this.formData.append('messageFromId', this.userId.toString());
    this.formData.append('companyId', (this.companyId ? this.companyId : 0).toString());
    this.formData.append('companyDetailId', (this.companyDetailId ? this.companyDetailId : 0).toString());
    this.formData.append('images', this.msgInputTxt);
    this.formData.append('imagesType', this.imageType);
    this.formData.append('messageTitle', this.msgInputTxt);
    this.formData.append('messageId', "0");
    this.formData.append('messageDate', tempDate);
    this.formData.append('companyReportDetailReadingId', '0');

    if(this.params.messageId > 0) this.formData.append('messageId', (this.params.messageId).toString());
    else this.formData.append('messageId', "0");
    this.messageServices.addMessages(this.formData).subscribe(res => {
      this.msgInputTxt = '';
      this.params.messageId = 0;
      this.isImageSaved = false;
      this.notFound = false;
      this.imageType = "";
      this.imageUrl = "";
      this.getMessage();
      this.toastr.success('', res.message, {
        timeOut: 2000
      });
      this.clearFormData();

      setTimeout(() => {
        /** spinner ends after 2 seconds */
        this.SpinnerService.hide();
      }, 500);
    }, error => {
      this.clearFormData();
      setTimeout(() => {
        /** spinner ends after 2 seconds */
        this.SpinnerService.hide();
      }, 500);
    })
  }
  

  clearFormData(){
    this.formData.delete("MessageImagesVideo");
    this.formData.delete("messageFromId");
    this.formData.delete("messageDescription");
    this.formData.delete("companyId");
    this.formData.delete("companyDetailId");
    this.formData.delete("images");
    this.formData.delete("imagesType");
    this.formData.delete("messageTitle");
    this.formData.delete("messageId");
    this.formData.delete("messageDate");
  }

  deleteMessage(id : number){
    let confirmation = confirm('Do you want to delete this Message?');
    if (confirmation) {
      this.messageServices.deleteMessages({messageId:id}).subscribe(res => {
        if(this.messagesList && this.messagesList.length > 0){
          this.notFound = false;
        }else{
          this.notFound = true;
        }
        this.getMessage();
        this.toastr.success('Successfully', 'Message Delete', {
          timeOut: 2000
        });
      })
    }
   
  }

  imageUrl : any;
  imageVideoType: any;
  fileChangeEvent(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();
      const file = fileInput.target.files[0];
      this.formData.append('MessageImagesVideo', file);
      reader.readAsDataURL(file);
      this.imageType = file.type.split('/')[1];
    
    reader.onload = (e: any) => {
      const image = new Image();
      image.src = e.target.result;
      this.imageUrl = String(reader.result).split(',')[1];
      
    }
    
  }
    

  }



   
}
