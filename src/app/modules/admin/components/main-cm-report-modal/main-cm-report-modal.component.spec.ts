import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCmReportModalComponent } from './main-cm-report-modal.component';

describe('MainCmReportModalComponent', () => {
  let component: MainCmReportModalComponent;
  let fixture: ComponentFixture<MainCmReportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainCmReportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCmReportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
