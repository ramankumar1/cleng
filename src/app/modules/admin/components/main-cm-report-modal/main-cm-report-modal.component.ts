import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { GlobalCodeService } from 'src/app/core/services/global-code.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { BaseUrl } from '../../../../config/url-config'
import { MessageService } from 'src/app/core/services/message.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment'
import html2canvas from 'html2canvas';
import { saveAs } from 'file-saver';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-main-cm-report-modal',
  templateUrl: './main-cm-report-modal.component.html',
  styleUrls: ['./main-cm-report-modal.component.scss']
})
export class MainCmReportModalComponent implements OnInit {
  @Input('cmpanyId') cmpanyId : number;
  // @Input() pdfReportUrl: any;
  @Input('companyReportDetailReadingId') companyReportDetailReadingId: number;
  @Output() handleClose = new EventEmitter;
  api = BaseUrl.apiUrl;
  fullCmReportList: any;
  msgInputTxt : '';
  msgList = [];
  notEmpty: boolean = false;
  emptyData: boolean = true;
  messagesList: any[] = [];
  readingId: number;
  companyId: number = 0;
  notFound: boolean = true;  
  editMsgData : any;
  userId: number;
  companyDetailId: number = 0;

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    autoplay: true,
    // navText: ['Previous', 'Next'],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 3,
      },
      768: {
        items: 3,
      },
      1024: {
        items: 4,
      },
      1280: {
        items: 4,
      },

    },
    nav: true
  }
  constructor(
    private globalCodeService: GlobalCodeService,
    private localStorageService: LocalStorageService,
    private messageServices: MessageService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  pdfReportUrl: any;
  assetId:any;
  companyNo: any;
  area: string = '';
  ngOnInit(): void {
    const userId = this.localStorageService.getUserCredentials();
    this.userId = userId.userId;
   
    this.readingId = this.companyReportDetailReadingId;
    this.companyId = this.cmpanyId;
    

    this.route.queryParams.subscribe(parms => {
      this.pdfReportUrl = parms.pdfReportUrl;
      this.companyNo = parms.companyNo;
      this.assetId = parms.assetId;
      this.area = parms.area;
    })

  }


  elementType = 'url';

  @ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;

  downloadImage(){
    let data = this.pdfReportUrl;
    data = data.split("/admin");
    const url = "/admin"+data[1];
    html2canvas(this.screen.nativeElement).then(canvas => {
      this.canvas.nativeElement.src = canvas.toDataURL();
      this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      this.downloadLink.nativeElement.download = 'qr_code.png';
      this.downloadLink.nativeElement.click();
    });
    this.router.navigateByUrl(url);
  }

  goback(){
    let data = this.pdfReportUrl;
    data = data.split("/admin");
    const url = "/admin"+data[1];
    this.router.navigateByUrl(url);
  }

}
