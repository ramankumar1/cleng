import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { CompanyService } from 'src/app/core/services/company.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { GlobalCodeService } from 'src/app/core/services/global-code.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit, OnDestroy {
  companyTable: string[] = ['Company', 'Assigned Users', 'Actions'];

  editData: any;
  addCompanyData: boolean = false;
  companyList: any;

  searchFilterSub: Subscription;
  constructor(
    private toastr: ToastrService,
    private companyService: CompanyService,
    private localStorageService: LocalStorageService,
    private router: Router,
    private SpinnerService: NgxSpinnerService,
    private globalService: GlobalCodeService
  ) { 
    this.searchFilterSub = this.globalService.searchFilter$.subscribe(value => {
      this.applyFilter(value);
    })
  }

  ngOnInit(): void {
    this.getAllCompany();
  }

  
  getAllCompany(){
    this.SpinnerService.show();
    this.companyService.getAllCompany({companyId:0, page:0, limit:0, orderBy:"CompanyName", orderByDescending:true, allRecords:true}).subscribe(res => {
      this.companyList = new MatTableDataSource(res.companiesResponseList);
      setTimeout(() => {
        /** spinner ends after 2 seconds */
        this.SpinnerService.hide();
      }, 500);
    }, error => {
    
    })
  }

  public applyFilter = (value: string) => {
    this.companyList.filter = value.trim().toLocaleLowerCase();
  }

  updateCompany(object){
    let userData = this.localStorageService.getUserCredentials();
    this.addCompanyData = true;
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "companyId": object.companyId,
        "companyName":object.companyName,    
        "actionBy": userData.userId,
        "addUpdateCompnayDetails": JSON.stringify(object.companyDetailList)
      }
    };
    this.router.navigate(["/admin/add-company"], navigationExtras)
  }


  addCompany(){
    
    let userData = this.localStorageService.getUserCredentials();
    this.addCompanyData = true;
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "companyId": 0,
        "companyName":"",    
        "actionBy": userData.userId,
        "addUpdateCompnayDetails":JSON.stringify([
          {
            "companyDetailId": 0,
            "actionType": 0,
            "companyNo": "",
            "companyArea": "",
            "assetId": "",
            "sapNo": "",
          }])
    }
    };
    this.router.navigate(["/admin/add-company"], navigationExtras)
  }

  deleteCompany(id: number){
    let confirmation = confirm('Do you want to delete this Row?');
    if (confirmation) {
      this.companyService.deleteCompany({companyId: id}).subscribe(res => {
        this.toastr.success('Successfully', 'Company Delete', {
          timeOut: 2000
        });
        this.getAllCompany();
        }, error => {
        })
    }
    
  }

  ngOnDestroy(){
    this.searchFilterSub.unsubscribe();
  }

}

