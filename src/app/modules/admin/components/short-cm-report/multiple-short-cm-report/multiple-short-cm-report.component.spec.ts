import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleShortCmReportComponent } from './multiple-short-cm-report.component';

describe('MultipleShortCmReportComponent', () => {
  let component: MultipleShortCmReportComponent;
  let fixture: ComponentFixture<MultipleShortCmReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleShortCmReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleShortCmReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
