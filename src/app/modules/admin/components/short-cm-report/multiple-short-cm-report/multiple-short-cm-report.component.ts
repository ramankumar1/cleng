import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-multiple-short-cm-report',
  templateUrl: './multiple-short-cm-report.component.html',
  styleUrls: ['./multiple-short-cm-report.component.scss']
})
export class MultipleShortCmReportComponent implements OnInit {
  @Input() multipleShortCMReportList = [];
  grouppedReports: any[] = [];
  readonly shortCMTable: string[] = [
    'CMP No', 'Area', 'Asset Id', 'Alarm Color',
    'Primary Issues', 'Secondary Issues',
    'Recommendations', 'Sap No', 'Priority'
  ];

  ngOnInit(): void {
    for (const group of Object.values(this.multipleShortCMReportList)) {
      group.sort((a, b) => a.reportDate - b.reportDate);
    }

    this.grouppedReports = Object.entries(this.multipleShortCMReportList);
  }
}
