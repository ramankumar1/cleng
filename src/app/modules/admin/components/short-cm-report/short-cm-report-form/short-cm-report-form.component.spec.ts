import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortCmReportFormComponent } from './short-cm-report-form.component';

describe('ShortCmReportFormComponent', () => {
  let component: ShortCmReportFormComponent;
  let fixture: ComponentFixture<ShortCmReportFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortCmReportFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortCmReportFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
