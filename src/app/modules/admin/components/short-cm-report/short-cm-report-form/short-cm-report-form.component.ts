import { Component, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ShortCmReprtRequestModel } from 'src/app/core/models/short-cm-report.model';

@Component({
  selector: 'app-short-cm-report-form',
  templateUrl: './short-cm-report-form.component.html',
  styleUrls: ['./short-cm-report-form.component.scss']
})
export class ShortCmReportFormComponent {
  @ViewChild('form') form: NgForm;
  @Input() formDetails = new ShortCmReprtRequestModel()
  @Output() formSubmit = new EventEmitter<void>();

  minDate = new Date('01-01-2021');
  maxDate = new Date();

  submit() {
    if(this.form.valid){
      this.formSubmit.emit();
    }
  }
}
