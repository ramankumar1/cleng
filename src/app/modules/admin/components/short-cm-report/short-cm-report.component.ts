import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { BaseUrl } from 'src/app/config/url-config';
import { ShortCmReprtRequestModel } from 'src/app/core/models/short-cm-report.model';
import { ExcelFormatService } from 'src/app/core/services/excel.service';
import { GlobalCodeService } from 'src/app/core/services/global-code.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { ReportWizardService } from 'src/app/core/services/report-wizard.service';
import { ShortCmReportFormComponent } from './short-cm-report-form/short-cm-report-form.component';

@Component({
  selector: 'app-short-cm-report',
  templateUrl: './short-cm-report.component.html',
  styleUrls: ['./short-cm-report.component.scss']
})
export class ShortCmReportComponent implements OnInit, OnDestroy {
  @ViewChild('TABLE') table: ElementRef;
  @ViewChild('shortCmReportForm') shortCMFormComponent: ShortCmReportFormComponent;


  api = BaseUrl.apiUrl;
  ShortCMTable: string[] = ['CMP No', 'Area', 'Asset Id', 'Alarm Color',
    'Primary Issues', 'Secondary Issues',
    'Recommendations', 'Sap No', 'Priority'];
  assignedCompany: any;
  userId: any;
  ShortCMReportList: any = [];
  companyId: number = 0;
  noAssign: boolean;
  assign: boolean;
  companyName: string;
  reportDate: string;
  companyReportDetailList: any;
  intValue: number;
  notFound: boolean = true;
  companyReportPDFData = [];
  accountType: any;
  shortCmData: boolean = true;
  searchFilterSub: Subscription;
  reportListDataForFilter: any;

  
  isMultiGenerateReport = false;
  multipleShortTermReport: any[] = [];

  formDetails = new ShortCmReprtRequestModel();
  constructor(
    private localStorageService: LocalStorageService,
    private globalService: GlobalCodeService,
    private SpinnerService: NgxSpinnerService,
    private reportWizardService: ReportWizardService,
    private toastr: ToastrService,
    private router: Router,
    private excelFormatService: ExcelFormatService,
  ) {
    this.searchFilterSub = this.globalService.searchFilter$.subscribe(value => {
      this.searchFilter(value);
    })
  }

  ngOnInit(): void {
    this.bindDropdown();
    const user = this.localStorageService.getUserCredentials();
    this.userId = user.userId;
    this.accountType = user.accountType.accountTypeName
    const compId = this.localStorageService.getCompanyId();
    if (compId.companyId == 0) {
      this.companyId = 0;
    } else {
      this.companyId = compId.companyId;
      this.companyName = compId.companyName;
    }

    this.getCompanyReportDetails();
   
  }
  getShortCMReportByDate() {
    const id = Number(this.companyId ? this.companyId : 0);
    const { startDate, endDate } = this.formDetails;
    this.globalService.getShortCMReportByDate(
      { companyId: id, page: 0, limit: 0, orderBy: "CreatedOn", orderByDescending: true, allRecords: true, startDate, endDate }
    ).subscribe(res => {
      if (res?.shortCMResponsesList) {
        this.shortCmData = false;
        this.isMultiGenerateReport = true;
      }

      this.ShortCMReportList = new MatTableDataSource(res?.shortCMResponsesList);
      this.SpinnerService.hide();
      const shortCMReport = res?.shortCMResponsesList;
      const sortedData = shortCMReport?.sort((a, b) => {
        const firstDate = +new Date(a.reportDate);
        const secDate = +new Date(b.reportDate);
        return secDate - firstDate;
      });
      this.multipleShortTermReport = this.groupedByDate(sortedData);
      this.shortCMFormComponent.form.resetForm();
    })
  }

  getCompanyReportDetails() {
    this.SpinnerService.show();
    const id = Number(this.companyId ? this.companyId : 0);
    this.globalService.getAssignedCompanyReportDetail({ companyId: id, page: 0, limit: 0, orderBy: "ReportDate", orderByDescending: true, allRecords: true }).subscribe(res => {
      this.companyReportDetailList = res.companyReportDetailResponses;
      let publishReportDate = '';
      let count = 0;
      if (this.companyReportDetailList.length > 0) {
        this.companyReportDetailList.forEach(data => {
          if (data.reportStatus == 'Publish') {
            count++;
            if (count == 1) publishReportDate = data.reportDate;
          }
        });

        this.storeTwelveMonthReportData(id, publishReportDate, '', 0, '');
      }

      this.reportListDataForFilter = res.companyReportDetailResponses;
      if (this.companyReportDetailList && this.companyReportDetailList.length > 0) {

        this.notFound = false;
        setTimeout(() => {
          /** spinner ends after 2 seconds */
          this.SpinnerService.hide();
        }, 500);

      } else {
        // this.companyName = null;
        this.notFound = true;
        setTimeout(() => {
          /** spinner ends after 2 seconds */
          this.SpinnerService.hide();
        }, 500);
        if (this.intValue == 1) {
          this.getCompanyReportDetails();
          this.intValue = 2;
        }

      }
    })
  }

  //Delete Draft Report and Save PDF
  deleteReport(id: number, archiveId: number, reportDate: string) {
    let companyReportDetailId = Number(id ? id : 0);
    let companyReportDetailArchivId = Number(archiveId ? archiveId : 0);
    let confirmation = confirm('Do you want to delete this Report?');
    if (confirmation) {
      if (this.ShortCMReportList) {
        if (this.ShortCMReportList.filteredData[0].reportDate == reportDate) {
          this.ShortCMReportList = "";
          this.shortCmData = true;
        }
      }
      this.reportWizardService.deleteReport({ companyReportDetailId: companyReportDetailId, companyReportDetailArchivId: companyReportDetailArchivId }).subscribe(res => {
        this.toastr.success('Successfully', res.message, {
          timeOut: 2000
        });
        this.getCompanyReportDetails();

      })
    }
  }

  //Edit Report
  editDraft(object) {
    if (this.accountType == 'Admin') {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          "reportDate": object.reportDate,
          "userId": object.userId,
          "companyId": this.companyId,
          "crdId": object.companyReportDetailId,
          "collectedBy": object.collectedBy,
          "companyName": this.companyName,
          "reportTitle": object.reportTitle
        }
      };
      this.router.navigate(["/admin/report-wizard"], navigationExtras);
    } else {
      this.toastr.warning('', 'Only the Admin can edit the report', {
        timeOut: 2000
      });
    }
  }

  viewFullCmReport(companyReportDetailId: number) {

    let index = this.ShortCMTable.indexOf('reportDate');
    if (index > -1) {
      this.ShortCMTable.splice(index, 1);
    }

    this.getShortCMReportList(companyReportDetailId);
  }

  getShortCMReportList(companyReportDetailId) {
    const id = Number(this.companyId ? this.companyId : 0);
    this.globalService.getShortCMReportList({ companyId: id, companyReportDetailId: companyReportDetailId, page: 0, limit: 0, orderBy: "CreatedOn", orderByDescending: true, allRecords: true }).subscribe(res => {
      this.ShortCMReportList = new MatTableDataSource(res.shortCMResponsesList);
      if (this.ShortCMReportList.filteredData) {
        this.shortCmData = false;
        this.isMultiGenerateReport = false;
        this.companyName = this.ShortCMReportList.filteredData[0].companyName;
        this.reportDate = this.ShortCMReportList.filteredData[0].reportDate;
      } else {
        this.shortCmData = true;
      }
    }, error => {

    })
  }

  bindDropdown() {
    const user = this.localStorageService.getUserCredentials();
    this.userId = user.userId;
    this.globalService.getAssignedCompany({ userId: this.userId }).subscribe(res => {
      this.assignedCompany = res.assignedUserCompanies;
      if (this.assignedCompany) {

        if (this.companyId == 0) {
          this.companyId = this.assignedCompany[0].companyId;
        }

        this.getCompanyReportDetails();
        this.assign = true;
        this.noAssign = false;
      } else {

        this.companyId = 0;
        this.assign = false;
        this.noAssign = true;
      }
    })
  }

  getByCompanyId(event) {
    this.shortCMFormComponent.form.resetForm();
    this.isMultiGenerateReport = false;
    
    this.companyId = event.target.value;
    this.shortCmData = true;
    const index = this.assignedCompany.findIndex(x => x.companyId == this.companyId);
    this.companyName = this.assignedCompany[index].companyName;
    this.localStorageService.storeCompanyId(this.assignedCompany[index]);

    this.ShortCMReportList = null;
    this.reportDate = "";

    this.getCompanyReportDetails();

  }

  searchFilter(searchTerm: string) {
    searchTerm = searchTerm.trim();
    const allReport = this.reportListDataForFilter;
    if (!searchTerm) {
      this.notFound = false;
      this.companyReportDetailList = this.reportListDataForFilter;
    }

    const searchedlist = allReport.filter((report) => {
      const reportDate = report.reportDate.toLowerCase().includes(searchTerm.toLowerCase());
      const reportTitle = report.reportTitle.toLowerCase().includes(searchTerm.toLowerCase());
      const status = report.reportStatus.toLowerCase().includes(searchTerm.toLowerCase());
      return reportDate || reportTitle || status;
    })
    this.notFound = searchedlist?.length == 0;
    this.companyReportDetailList = searchedlist;
  }

  exportTOExcel() {
    const excelData = this.ShortCMReportList.filteredData ? this.ShortCMReportList.filteredData : this.ShortCMReportList;
    const report = this.groupedByDate(excelData);
    this.excelFormatService.generateDateByExcel(report, this.companyName);
  }

  groupedByDate(report) {
    let groupDetails = report?.reduce((group, report) => {
      group[report.reportDate] = [...group[report.reportDate] || [], report];
      return group;
    }, {});
    return groupDetails
  }

  storeTwelveMonthReportData(companyId, reportDate, engineerName, companyReportDetailId, collectedBy) {
    const data = {
      companyId: companyId,
      reportDate: reportDate,
      engineerName: engineerName,
      companyReportDetailId: 0,//companyReportDetailId,
      collectedBy: collectedBy
    }
    this.localStorageService.storeTwelveMonthData(data);
  }


  ngOnDestroy() {
    this.searchFilterSub.unsubscribe();
  }

  print(companyReportDetailId: number): void {

    this.getShortCMReportList(companyReportDetailId);
    setTimeout(function () {
      let printContents, popupWin, printbutton;
      printbutton = document.getElementById('inputprintbutton').style.display = "none";
      printContents = document.getElementById('shorcmtablediv').innerHTML;
      popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
      popupWin.document.open();

      popupWin.document.write(`
    <html>
      <head>
    
        <title>Print tab</title>
        <style media="print">
        * {
          -webkit-print-color-adjust: exact !important; /*Chrome, Safari */
          color-adjust: exact !important;  /*Firefox*/
          }
          .mat-table{
            border: 1px solid #dee2e6;
           }
          .mat-table > thead > tr{
           background-color:#000;  
          }
          .mat-table > thead > tr > th{
            color:#fff;  padding:2px;
            font-size:14px;    
            font-family: Roboto, "Helvetica Neue", sans-serif;
           }
           .mat-table > thead > tr > th:first-child{
            width:65px;
           }
           .mat-table > thead > tr > th:nth-child(2)
           {
            width:55px;
           }
           .mat-table > thead > tr > th:nth-child(3)
           {
            width:55px;
           }
           .mat-table > thead > tr > th: th:nth-child(4)
           {
            min-width:100px;
           }
           .mat-table > thead > tr > th: th:nth-child(5)
           {
            width:100px;
           }
           .mat-table > thead > tr > th: th:nth-child(6)
           {
            width:70px;
           }
           .mat-table > tbody > tr > td{
            padding:6px 2px;
            font-size:12px;
            vertical-align:middle;
            text-align:center;
            font-family: Roboto, "Helvetica Neue", sans-serif;
            font-weight:400;
           
           }
           .float-right{
             float: right;
           }
           .card-header{
            margin:0 0 20px 0;
            font-family: Roboto, "Helvetica Neue", sans-serif;
           }
           .mat-table > tbody > tr:nth-child(even) {
            background: #f5f5f5;
        }
        .mat-table > tbody > tr:nth-of-type(odd) {
          background-color: rgba(0, 0, 0, 0.05);
      }
        //........Customized style.......
        </style>
      </head>
  <body onload="window.print();window.close()">${printContents}</body>
    </html>`
      );
      printbutton = document.getElementById('inputprintbutton').style.display = "inline-block";
      popupWin.document.close();
    }, 2000);

  }

}
