import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ReportDetailsForm } from 'src/app/core/models/report-wizard.model';
import { ReportWizardService } from 'src/app/core/services/report-wizard.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { GlobalCodeService } from 'src/app/core/services/global-code.service';
import { UserService } from 'src/app/core/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm, NgModel } from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'report-details-form',
  templateUrl: './report-details-form.component.html',
  styleUrls: ['./report-details-form.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class ReportDetailsFormComponent implements OnInit {
  @Output() onReportSave = new EventEmitter<ReportDetailsForm>();
  @Output() onFilterChange = new EventEmitter<ReportDetailsForm>();

  userId: number;
  companies: any = [];
  engineers: any = [];
  detailsForm: ReportDetailsForm = {
    companyId: 0,
    userId: 0,
    reportDate: null,
    crdId: 0,
    collectedBy: null,
    reportTitle: null
  };


  constructor(
    private globalCodeService: GlobalCodeService, 
    private localStorageService: LocalStorageService,
    private userService: UserService,
    private route: ActivatedRoute,
    ) { }

  ngOnInit(): void {
    this.detailsForm = {
      reportDate: null,
      userId: 0,
      companyId: 0,
      crdId: 0,
      collectedBy: null,
      reportTitle: null
    }

    this.getUserByAccount();
    this.getCustomerName();

      this.route.queryParams.subscribe(params => {
        if(params){
          if(this.detailsForm.companyId == 0 && this.detailsForm.userId == 0 && this.detailsForm.crdId == 0 && this.detailsForm.reportDate == null){
            this.detailsForm.companyId = params.companyId;
            this.detailsForm.reportDate = params.reportDate;
            this.detailsForm.userId = params.userId;
            this.detailsForm.crdId = params.crdId;
            this.detailsForm.collectedBy = params.collectedBy;
            this.detailsForm.reportTitle =params.reportTitle
          }
        }
        
    })
  }

  
  getCustomerName(){
    const user = this.localStorageService.getUserCredentials();
    this.userId = user.userId;
    this.globalCodeService.getAssignedCompany({userId: this.userId}).subscribe(res => {
      this.companies = res.assignedUserCompanies;
    })   
  }

  getUserByAccount(){
    this.userService.getUserByAccountType({accountTypeName:"Admin", page:0, limit:0, orderBy:"CreatedOn", orderByDescending:true, allRecords:true}).subscribe(res => {
      this.engineers = res.userResponsesList;
      this.engineers = this.globalCodeService.sortByAlphabetical(this.engineers,'contactName');
    }, error => {
    })
  }

  saveCompanyReportDetails() {
    this.onReportSave.emit(this.detailsForm);
  }

  filterChanged(){
    const index = this.companies.findIndex(x => x.companyId == this.detailsForm.companyId);
    if(index > -1){
      this.detailsForm.companyName = this.companies[index].companyName;
    }
    this.onFilterChange.emit(this.detailsForm);
  }

  isInvalidField(reportDetailsForm: NgForm, inputControl: NgModel): boolean {
    return (reportDetailsForm.submitted || inputControl.touched) && inputControl.invalid;
  }
}
