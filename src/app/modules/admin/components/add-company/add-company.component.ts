import { Component, OnInit, Input } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { CompanyService } from 'src/app/core/services/company.service';
import { CompanyModal, CompanyDetails } from 'src/app/core/models/company.model';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { GlobalCodeService } from 'src/app/core/services/global-code.service';
import { BaseUrl } from 'src/app/config/url-config';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit {
  @Input('editData') editData: any;
  api = BaseUrl.apiUrl;
  messageIcon: boolean = false;
  loadComponent: boolean = false;
  addCompanyData : CompanyModal = {
    companyName : null,
    companyId: null,
    addUpdateCompnayDetails: [],
    actionBy: null
  }

  companyDetailData: CompanyDetails = {
    rownum:0,
    companyDetailId: 0,
    actionType:0,
    companyNo: null,
    companyArea: null,
    assetId: null,
    sapNo: null,
    assetImage: "",
    imageType: "",
    isImage: false,
  }

  public companyFields :CompanyDetails[] = [{
    rownum:0,
    companyDetailId: 0,
    actionType:0,
    companyNo: null,
    companyArea: null,
    assetId: null,
    sapNo: null,
    assetImage: "",
    imageType: "",
    isImage: false
  }];


  constructor(
    private toastr: ToastrService,
    private companyService: CompanyService,
    private localStorage: LocalStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private globalService: GlobalCodeService,
    private SpinnerService: NgxSpinnerService
  ) { }
  
  storeData : any
  ngOnInit(): void {
    
    this.route.queryParams.subscribe(params => {
      this.storeData = params;

      this.addCompanyData.companyName = params["companyName"];
      this.addCompanyData.companyId = params["companyId"];
      this.addCompanyData.actionBy = params["actionBy"];
      this.companyFields=JSON.parse(params["addUpdateCompnayDetails"]); 
      this.companyFields = this.globalService.sortByNumeric(this.companyFields,"companyDetailId");
      for (var i = 0; i < this.companyFields.length; i++) {
        this.companyFields[i].actionType = 0;
        this.companyFields[i].rownum = i+1;
       
     }

      this.addCompanyData.addUpdateCompnayDetails=this.companyFields;
      if(this.addCompanyData.addUpdateCompnayDetails[0].companyDetailId > 0){
        this.messageIcon = true;
      }
      for(var i = 0; i < this.companyFields.length; i++){
        this.companyFields[i].assetImage = "";
        this.companyFields[i].imageType = "";
      }
    });
  }

  //Upload image
  uploadPdf(fileInput: any, index: number){
    const reader = new FileReader();
    const file = fileInput.target.files[0];
    reader.readAsDataURL(file);
    const fileType = file.type.split('/')[1];
    reader.onload = (fileInput: any) => {
      const image = new Image();
      image.src = fileInput.target.result;
      const img = String(reader.result).split(',')[1];
      this.companyFields[index].assetImage = img;
      this.companyFields[index].imageType = fileType;
    }
  }

  addCompany(form : NgForm){
    if (this.addCompanyData.companyId > 0) {
      this.updateCompany();
    }else {
      if(form.valid){
        this.SpinnerService.show();
        let userData = this.localStorage.getUserCredentials();
          this.addCompanyData.actionBy = userData.email;
          this.addCompanyData.companyId = 0;
          this.companyFields = this.companyFields.filter(x => x.actionType != 1);
          this.companyFields = this.companyFields.filter(x => x.companyNo != null);
          for(let i = 0; i<this.companyFields.length; i++){
            if(this.companyFields[i].assetImage){
              this.companyFields[i].isImage = true;
            }
          }
          this.addCompanyData.addUpdateCompnayDetails = this.companyFields;
        this.companyService.addCompany(this.addCompanyData).subscribe(res => {
          this.SpinnerService.hide();
          setTimeout(() =>{
            this.toastr.success('Successful', 'Company Created', {
              timeOut: 2000
            });
            this.router.navigateByUrl("/admin/company");
          }, 500)
         
        }, error => {
          
        })
        
      }
    }
   
  }

  updateCompany(){
    this.SpinnerService.show();
    let userData = this.localStorage.getUserCredentials();
    this.addCompanyData.actionBy = userData.email;
    this.addCompanyData.companyName = this.addCompanyData.companyName;
    this.addCompanyData.companyId = Number(this.addCompanyData.companyId);
    for(let i = 0; i<this.companyFields.length; i++){
      if(this.companyFields[i].assetImage){
        this.companyFields[i].isImage = true;
      }
    }
    this.addCompanyData.addUpdateCompnayDetails=this.companyFields;
    this.companyService.UpdateCompany(this.addCompanyData).subscribe(res => {setTimeout(() =>{
      this.SpinnerService.hide();
      this.toastr.success('Successful', 'Company Update', {
        timeOut: 2000
      });
      this.router.navigateByUrl("/admin/company");
    }, 500)
     
    }, error => {
    })
  }

  addMoreField(){
    var max =Math.max.apply(Math, this.companyFields.map(function(o) { return o.rownum; }))

      this.companyFields.push({
      rownum:max+1,
      companyDetailId: 0,
      actionType:0,
      companyNo: null,
      companyArea: null,
      assetId: null,
      sapNo: null,
      assetImage: "",
      imageType: "",
      isImage: false
    });
  }

  removeField(i: number){
    var index=this.companyFields.findIndex(x => x.rownum === i)
    this.companyFields[index].actionType=1;
    
  }
  filterItemsOfType(){
    
    return this.companyFields.filter(x => x.actionType === 0);
    
  }
  windowScroll(){
    window.scrollTo(0, 0);
  }

  messageId: any
  messageClickIcon(object){
    this.loadComponent = true;
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "companyDetailId": object.companyDetailId,
        "companyId" : this.addCompanyData.companyId,
        "companyNo": object.companyNo,
        "assetId": object.assetId
      }
    };
    this.router.navigate(["/admin/messages"], navigationExtras)
  }

}
