import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrgIdListComponent } from './brgId-list.component';

describe('BrgIdListComponent', () => {
  let component: BrgIdListComponent;
  let fixture: ComponentFixture<BrgIdListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrgIdListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrgIdListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
