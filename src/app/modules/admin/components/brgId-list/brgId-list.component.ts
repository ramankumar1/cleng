import { Component, OnInit } from '@angular/core';
import { GlobalCodeService } from 'src/app/core/services/global-code.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-brgId-list',
  templateUrl: './brgId-list.component.html',
  styleUrls: ['./brgId-list.component.scss']
})
export class BrgIdListComponent implements OnInit {

  brgType: any[] = [];
  brgTypeDataForFilter: any[] = [];

  codeName: string = "";
  globalCodeCategoryId: number;
  notFound: boolean = false;

  searchFilterSub: Subscription;
  constructor(
    private globalCodeService: GlobalCodeService,
  ) { 
    this.searchFilterSub = this.globalCodeService.searchFilter$.subscribe(value => {
      this.searchFilter(value);
    })
  }

  ngOnInit(): void {
    this.getBrgId();
  }

  getBrgId(){
    this.globalCodeService.getGlobalCodeCategory({ name: "BrgId" }).subscribe(res => {
      this.brgType = res.data.globalCodeMainResponse.globalCodeResponse;
      this.brgTypeDataForFilter = this.brgType;
      if(this.brgType.length > 0) this.globalCodeCategoryId = this.brgType[0].globalCodeCategoryId;
      this.brgType = this.globalCodeService.sortByAlphabetical(this.brgType,'codeName');
    })
  }

  createGlobalCode(){
    let params = {
      "globalCodeCategoryId": this.globalCodeCategoryId,
      "codeName": this.codeName,
      "description": this.codeName,
      "isActive": true
    }
    this.globalCodeService.createGlobalCode(params).subscribe(res => {
      this.getBrgId();
    })
  }

  searchFilter(searchTerm: string){
    searchTerm = searchTerm.trim();
    const allReport = this.brgTypeDataForFilter;
    if(!searchTerm){
      this.notFound = false;
      this.brgType = allReport;
    }
  
    const searchedlist = allReport.filter((type) => {
      const codeName = type.codeName.toLowerCase().includes(searchTerm.toLowerCase());
      const desc = type.description.toLowerCase().includes(searchTerm.toLowerCase());
      return codeName || desc;
    })
    this.notFound = searchedlist?.length == 0;
    this.brgType = searchedlist;
  }
  
  ngOnDestroy(){
    this.searchFilterSub.unsubscribe();
  }

}
