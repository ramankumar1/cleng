import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullCmReportDetailComponent } from './full-cm-report-detail.component';

describe('FullCmReportDetailComponent', () => {
  let component: FullCmReportDetailComponent;
  let fixture: ComponentFixture<FullCmReportDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullCmReportDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullCmReportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
