import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { UserService } from 'src/app/core/services/user.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { GlobalCodeService } from 'src/app/core/services/global-code.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {
  userTable: string[] = ['Contact Name', 'User Name', 'Admin or Standard User', 'Email', 'Actions'];
  userData: any;
  editData: any;
  addUserForm:boolean = false;

  searchFilterSub: Subscription;
  constructor(
    private toastr: ToastrService,
    private userService: UserService,
    private localStorageService: LocalStorageService,
    private router: Router,
    private SpinnerService: NgxSpinnerService,
    private globalService: GlobalCodeService
  ) {
    this.searchFilterSub = this.globalService.searchFilter$.subscribe(value => {
      this.applyFilter(value);
    })
   }

  ngOnInit(): void {
    this.getAllUser();
  }

  getAllUser(){
    this.SpinnerService.show();
    this.userService.getAllUsers({userId:0, page:0, limit: 0, orderBy: "CreatedOn", orderByDescending: true, allRecords: true}).subscribe(res => {
      this.userData = new MatTableDataSource(res.userResponsesList);
      setTimeout(() => {
        /** spinner ends after 2 seconds */
        this.SpinnerService.hide();
      }, 500);
    })
  }

  public applyFilter = (value: string) => {
    this.userData.filter = value.trim().toLocaleLowerCase();
  }

  deleteUser(id: number){
    let confirmation = confirm('Do you want to delete this User?');
    if (confirmation) {
      this.userService.deleteUser({userId:id }).subscribe(res => {
        this.getAllUser();
        this.toastr.success('Successfully', 'User Delete', {
          timeOut: 2000
        });
        }, error => {
        })
    }
  }

  updateUser(object) {
    let userData = this.localStorageService.getUserCredentials();
    this.addUserForm = true;
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "userId": object.userId,
        "actionBy": userData.UserId,
        "userName": object.userName,
        "phoneNumber": object.phoneNumber,
        "contactName": object.contactName,
        "email": object.email,
        "assignCompanyIdRequests": JSON.stringify(object.userCompany),
        "accountTypeId": object.accountType.accountTypeId
      }
    };
    this.router.navigate(["/admin/add-user"], navigationExtras);

  }

  ngOnDestroy(){
    this.searchFilterSub.unsubscribe();
  }

}
