import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullCmReportListComponent } from './full-cm-report-list.component';

describe('MainCmReportComponent', () => {
  let component: FullCmReportListComponent;
  let fixture: ComponentFixture<FullCmReportListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullCmReportListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullCmReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
