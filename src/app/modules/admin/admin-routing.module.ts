import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { CompanyComponent } from './components/company/company.component';
import { AddCompanyComponent } from './components/add-company/add-company.component';
import { ReportWizardComponent } from './components/report-wizard/report-wizard.component';
import { ShortCmReportComponent } from './components/short-cm-report/short-cm-report.component';
import { FullCmReportComponent } from './components/full-cm-report/full-cm-report.component';
import { PrescriptiveMaintenanceComponent } from './components/prescriptive-maintenance/prescriptive-maintenance.component';
import { FullCmReportListComponent } from './components/full-cm-report-list/full-cm-report-list.component';
import { MessagesComponent } from './components/messages/messages.component';
import { FullCmReportDetailComponent } from './components/full-cm-report-detail/full-cm-report-detail.component';
import { MainCmReportModalComponent } from './components/main-cm-report-modal/main-cm-report-modal.component';
import { AuthGuard } from 'src/app/core/guard/auth.guard';
import { BrgIdListComponent } from './components/brgId-list/brgId-list.component';

const adminRoutes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full"
      },
      {
        path: "dashboard",
        component: DashboardComponent,
      },
      {
        path: "user-list",
        component: UserListComponent,
        canActivate: [AuthGuard], 
        data: { roles: ["Admin"] }
      },
      {
        path: "add-user",
        component: AddUserComponent,
        canActivate: [AuthGuard], 
        data: { roles: ["Admin"] }
      },
      {
        path: "company",
        component: CompanyComponent,
        canActivate: [AuthGuard], 
        data: { roles: ["Admin"] }
      },
      {
        path: "add-company",
        component: AddCompanyComponent,
        canActivate: [AuthGuard], 
        data: { roles: ["Admin"] }
      },
      {
        path: "report-wizard",
        component: ReportWizardComponent,
        canActivate: [AuthGuard], 
        data: { roles: ["Admin"] }
      },
      {
        path: "short-cm-report",
        component: ShortCmReportComponent,
      },{
        path: "full-cm-report",
        component: FullCmReportComponent,
      },{
        path: "full-cm-report-list",
        component: FullCmReportListComponent,
      },{
        path: "prescriptive-maintenance",
        component: PrescriptiveMaintenanceComponent,
      },
      
      {
        path: "messages",
        component: MessagesComponent,
      },
      {
        path: "brg-setup",
        component: BrgIdListComponent,
        canActivate: [AuthGuard], 
        data: { roles: ["Admin"] }
      },
      {
        path: "full-cm-report-detail",
        component: FullCmReportDetailComponent,
      },
      {
        path: "qr-code",
        component: MainCmReportModalComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
