export interface AuthModel {
    email?: string,
    password?: string,
    newPassword?:string,
    confirmPassword?: string,
    userName?:string
}