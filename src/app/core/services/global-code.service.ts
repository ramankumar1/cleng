import { Injectable, OnInit } from '@angular/core';
import { BaseUrl } from '../../config/url-config'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalCodeService{
  private subject = new Subject<any>();
  // private subjectPrint = new Subject<any>();
  api = BaseUrl.baseApiUrl;
  constructor(private http: HttpClient,
    private _localStorageService: LocalStorageService) { }


  searchFilter = new Subject<string>();
  searchFilter$ = this.searchFilter.asObservable();

  getGlobalCodeCategory(data): Observable<any> {
    return this.http.post<any>(this.api + 'GlobalCodesAPI/GetGlobalCodeByCategoryName', data);
  }

  getAllGlobalCode(data): Observable<any> {
    return this.http.post<any>(this.api + 'GlobalCodesAPI/GetAllRecords', data);
  }

  createGlobalCode(data): Observable<any> {
    return this.http.post<any>(this.api + 'GlobalCodesAPI/Create', data );
  }

  // Dashboard Api's
  getAssignedCompany(data): Observable<any> {
    return this.http.post<any>(this.api + 'DashboardAPI/GetAssignedCompanies', data);
  }

  getCompanyDetailReadings(data): Observable<any> {
    return this.http.post<any>(this.api + 'DashboardAPI/GetCompanyWithCompanyDetailReadings', data);
  }

  getCompanyDetailPopOut(data): Observable<any> {
    return this.http.post<any>(this.api + 'DashboardAPI/GetCompanyDetailPopOut', data);
  }

  companyDashboardForLastTwelveMonths(data): Observable<any> {
    return this.http.post<any>(this.api + 'DashboardAPI/GetCompanyDashboardForLastTwelveMonths', data);
  }


  // ShortCmReport Api
  getShortCMReportList(data) {
    return this.http.post<any>(this.api + 'FullAndShortCMReport/GetShortCMReort', data);
  }

  // FullCmReport APi
  getFullCMReportList(data) {
    return this.http.post<any>(this.api + 'FullAndShortCMReport/GetFullCMReort', data);
  }

 // FullCmReportDetail APi
  getFullCMReportDetail(data) {
    return this.http.post<any>(this.api + 'FullAndShortCMReport/GetFullCmReportDetails', data);
  }

  getOverAllTwelveMonthsDetail(data) {
    return this.http.post<any>(this.api + 'FullAndShortCMReport/GetFullCmOverAllReportDetails', data);
  }

  getAssignedCompanyReportDetail(data) {

    return this.http.post<any>(this.api + 'FullAndShortCMReport/GetCompanyReportDetail', data);
  }

  getShortCMReportByDate(data) {
    return this.http.post<any>(this.api + 'FullAndShortCMReport/GetShortCMReortByDate', data);
  }

  saveCMSurveyPDF(data) {
    return this.http.post<any>(this.api + 'FullAndShortCMReport/SaveCMSurveyPDF', data);
  }

  // Prescriptive Maintenance Api
  getPrescriptiveMaintenance(data) {
    return this.http.post<any>(this.api + 'PrescriptiveMaintenanceAPI/GetPrescriptiveMaintenance', data);
  }
  getFaultCountForLastMonths(data) {
    return this.http.post<any>(this.api + 'PrescriptiveMaintenanceAPI/GetFaultCountForLastMonths', data);
  }
  
  // data is always of Array type
  // sortBy is used to sort the array and always present in Array
  sortByAlphabetical(data, sortBy: string) {
    if(data){
      let items = data.sort(function (a, b) {
        let t1 = a[sortBy].toUpperCase();
        let t2 = b[sortBy].toUpperCase();
        return t1.localeCompare(t2);
      });
      return items;
    }

  }

  sortByNumeric(data, sortBy: string) {
    if(data){
        let items = data.sort(function (a, b) {
        let t1 = a[sortBy];
        let t2 = b[sortBy]; 
        return t1 - t2;
      });
    return items;
    }

  }

  sortByCMPNo(data){
    if(data){
      data.sort((a, b) => {
        const first = a.companyDetail?.companyNo;
        const second = b.companyDetail?.companyNo;
        let firstVal = this.processText(first);
        let secondVal = this.processText(second);

        return firstVal - secondVal || first.localeCompare(second);

      });
    }
    
  }

  processText(inputText) {
    const numberArray = inputText.match(/\d+/);
    if (numberArray) return +numberArray[0];

    return null;
  }

  sendData(id: number) {
    this.subject.next({ checkId: id });
  }

  clearData() {
    this.subject.next();
  }

  getData(): Observable<any> {
    return this.subject.asObservable();
  }




  // sendDataPrint(value: boolean) {
  //   this.subjectPrint.next({ checkValue: value });
  // }

  // clearDataPrint() {
  //   this.subjectPrint.next();
  // }

  // getDataPrint(): Observable<any> {
  //   return this.subjectPrint.asObservable();
  // }


  dateBasicDataFetchOnDashboard(data): Observable<any> {
    return this.http.post<any>(this.api + 'DashboardAPI/GetCompanyWithCompanyDetailReadingsByDate', data);
  }
  
}
