import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { Workbook, Worksheet } from 'exceljs';
import * as fs from 'file-saver';
@Injectable({
  providedIn: 'root',
})
export class FullCMReprotDetailsExcelFormatService {

  constructor(public datepipe: DatePipe){}

  generateLastTwelveMonthsReport(companyName, reportDate, headerDetails, summaryDetails) {
    let keys = [];

    for(let key in headerDetails) {
      if(key != 'CompanyDetailId') {
        keys.push(key);
      }
    }
    const arr = keys.map(k => (k != 'CompanyNo' && k != 'CompanyArea' && k != 'AssetId')? k : null);
    const headerKeys = arr.slice(3);
    const header = ['CMP NO', 'AREA', 'ASSETS'].concat(headerKeys);

    // Mapping data
    const data = this.mappingData(keys, summaryDetails);

    let workbook = new Workbook();

    // Create workbook and worksheet
    const worksheet = workbook.addWorksheet('Full CM Report Details');

    // Add Row and formatting
    const titleRow = worksheet.addRow(['CONDITION MONITORING PROGRAM REPORT']);
    worksheet.addRow([]);
    const titleSecondRow = worksheet.addRow([`Last 12 months up to ${this.datepipe.transform(reportDate, 'dd/MM/yyyy')}`]);
    worksheet.addRow([]);
    const titleThirdRow = worksheet.addRow([`Customer / Area: ${companyName}`]);
    worksheet.addRow([]);

    titleRow.font = { size: 20, underline: 'double', bold: true };
    titleSecondRow.font = { size: 15, underline: 'double', bold: true };
    titleThirdRow.font = { size: 15, underline: 'double', bold: true };

    // Blank Row
    worksheet.addRow([]);

    // Add Header Row
    const headerRow = worksheet.addRow(header);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF' },
        bgColor: { argb: 'FFFFFF' }
      };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });

    // Add Data and Conditional Formatting
    data.forEach((d) => {
      const row = worksheet.addRow(d);
      for(let i = 0; i <= keys.length; i++) {
        const qty = row.getCell(i+1);
        if(i > 2) {
          let cellColor = "FFFFFF";
          if (qty.value == "Yellow") {
            cellColor = "FFFF00"
          }else if(qty.value == "Red"){
            cellColor = "FF0000";
          }else if(qty.value == "Green"){
            cellColor = "2A8947";
          }else if(qty.value == "N/R"){
            cellColor = "F9F9F9";
          }else {
            cellColor = "FFFFFF";
          }
          
          qty.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: cellColor }
          };
          qty.value = ' ';
        }
        if(keys.length > i) qty.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      }
    });

    this.setColumnSize(worksheet);
    worksheet.addRow([]);

    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'Full CM Report Details.xlsx');
    });
  }

  setColumnSize(worksheet: Worksheet) {
    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 40;
  }

  mappingData(keys, data){
    let excelData = [];
    data.map(data => {
      data.excelData = keys.map(k => data[k]);
      excelData.push(data.excelData);
    });
    return excelData;
  }
}
